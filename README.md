# PROGRAMAS_CVS_GITLAB

### PROGRAMAS - RECURSOS EDUCATIVOS ABIERTOS - UTPL

Proyecto para el control de versionamiento de Programas desarrollados como Recursos Educativos Abiertos (REA) en la Universidad Técnica Particular de Loja; modalidades: 

+ Clásica y 
+ M.a.D.

> ## Datos del Proyecto
>
> 1.    Autor: Pedro Daniel Irene Robalino
> 2.    Cargo: Docente - UTPL
> 3.    Asignaturas: 
>       * Lógica de la Programación. 
>       * Fundamentos de la Programación.  
>       * Programación de Algoritmos. 
>       * Arquitectura de Aplicaciones.
> 4.    Fecha de creación: 03-Ene.-2020

