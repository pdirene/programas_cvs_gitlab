package EXCEPCIONES;

public class MiExcepcionPropia_A extends Exception {
    // Constructor
    public MiExcepcionPropia_A()
    {
        this("Error de División para cero"); // Llama al otro constructor sobre cargado
    }

    // constructor
    public MiExcepcionPropia_A(String msjError)
    {
        super(msjError + " aqui"); // Llama al constructor de la super clase Exception
    }
}
