package EXCEPCIONES;

import java.util.Scanner;

public class ProbandoExcepcionPropia {

    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);
        int numero1 = teclado.nextInt();
        int numero2 = teclado.nextInt();
        int division;
        try {
            if (numero2!=0)
                division = numero1 / numero2;
            else
                throw new MiExcepcionPropia_A();
        } catch (Exception excepcion) {
            excepcion.printStackTrace();
        }
    }
}
