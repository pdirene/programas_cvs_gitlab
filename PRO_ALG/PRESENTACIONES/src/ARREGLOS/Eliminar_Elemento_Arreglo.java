/**
 * Programa para buscar un valor dado, y eliminarlo del arreglo
 * @author Pedro Daniel Irene Robalino
 * @version 1.0.0
 */

package ARREGLOS;
import java.util.Arrays;

public class Eliminar_Elemento_Arreglo {

    /**
     * Metodo de busqueda Lineal: busca secuencialmente en cada elemento
     * del arreglo hasta encontrar el elemento buscado o hasta llegar al final.
     * @param arreglo Arreglo sobre el cual se buscada el valorBuscado
     * @param valorBuscado valor que se comparara con cada elemento del arreglo
     * @return La posicion en el arreglo, del valorBuscado
     */
    public static int busquedaLineal(int arreglo[], int valorBuscado) {
        int posicion = -1;
        for (int i = 0; i < arreglo.length; i++)
            if (valorBuscado == arreglo[i]){
                posicion =  i ;
                eliminarElemento(arreglo, posicion);
                break;
            }
        return posicion; //Retorna -1 si no encuentra el valorBuscado
    }

    public static void eliminarElemento(int arreglo[], int posicion) {
        for (int i = posicion; i < arreglo.length - 1; i++)
            arreglo[i] = arreglo[i+1];
        arreglo[arreglo.length - 1] = 0;
    }

    public static void main(String[] args) {
        int[] arreglo = {23, 25, 29, 42, 44, 46, 48, 49, 57, 59, 68, 74, 75, 85, 97};
        System.out.println("ARREGLO ORIGEN:");
        System.out.println(Arrays.toString(arreglo));
        int valorBuscado = 23;
        System.out.printf("%nElemento a eliminar: [%d]%n", valorBuscado);
        int posicionElemento = busquedaLineal(arreglo, valorBuscado);
        System.out.println(posicionElemento >= 0
                ? "Elemento [" + (valorBuscado) + "] eliminado de la posicion: ["
                + posicionElemento + "]" : "ELEMENTO [" + (valorBuscado) + "] NO ENCONTRADO");
        System.out.println("ARREGLO FINAL:");
        System.out.println(Arrays.toString(arreglo));
    }
}
