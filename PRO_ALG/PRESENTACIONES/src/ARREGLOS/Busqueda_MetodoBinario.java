/**
 * Programa para buscar un valor dado, en un arreglo, con el metodo de busqueda
 * Binario
 * @author Pedro Daniel Irene Robalino
 * @version 1.0.0
 */

package ARREGLOS;
import java.util.Arrays;

public class Busqueda_MetodoBinario {

    /**
     * Metodo de busqueda Binario (2), que divide el arreglo en sub-arreglos
     * desde el centro a la derecha o izquierda, si no halla el elemento
     * en el centro de cada sub-arreglo.
     * @param arreglo Arreglo sobre el cual se buscada el valorBuscado
     * @param valorBuscado valor que se comparara con cada elemento del arreglo
     * @return La posicion en el arreglo, del valorBuscado
     */
public static int busquedaBinaria(int arreglo[], int valorBuscado) {
    int centro, inf = 0, sup = arreglo.length - 1;
    while (inf <= sup) {
        centro = (sup + inf) / 2; //Para preguntar si el elemento central, es el valorBuscado
        if (arreglo[centro] == valorBuscado) {
            return centro; //Termina el ciclo, si encuentre el valorBuscado
        } else if (valorBuscado < arreglo[centro]) {
            sup = centro - 1; //Toma sub-arreglo del centro hasta el inicio (derecha)
            System.out.println(Arrays.toString(Arrays.copyOfRange(arreglo, inf, sup+1)));
        } else {
            inf = centro + 1; //Toma sub-arreglo del centro hasta el final (izquierda)
            System.out.println(Arrays.toString(Arrays.copyOfRange(arreglo, inf, sup+1)));
        }
    }
    return -1; //Retorna -1 si no encuentra el valorBuscado
}

    public static void main(String[] args) {
        int [] arreglo = {23, 25, 29, 42, 44, 46, 48, 49, 57, 59, 68, 74, 75, 85, 97};
        System.out.println("ARREGLO ORIGEN:");
        System.out.println(Arrays.toString(arreglo));
        int valorBuscado = 75;
        System.out.printf("%nElemento a buscar: [%d]%n", valorBuscado);
        System.out.println("\nBUSCANDO EL ELEMENTO EN:");
        System.out.println(Arrays.toString(arreglo));
        int posicionElemento = busquedaBinaria(arreglo, valorBuscado);
        System.out.println(posicionElemento > 0
                ? "Elemento [" + (valorBuscado) + "] encontrado en la posicion: ["
                + posicionElemento + "]" : "ELEMENTO [" + (valorBuscado) + "] NO ENCONTRADO");
    }
}
