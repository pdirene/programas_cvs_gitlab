/**
 * Programa para buscar un valor dado, en un arreglo, con el metodo lineal
 * @author Pedro Daniel Irene Robalino
 * @version 1.0.0
 */

package ARREGLOS;
import java.util.Arrays;

public class Busqueda_MetodoLineal {

    /**
     * Metodo de busqueda Lineal: busca secuencialmente en cada elemento
     * del arreglo hasta encontrar el elemento buscado o hasta llegar al final.
     * @param arreglo Arreglo sobre el cual se buscada el valorBuscado
     * @param valorBuscado valor que se comparara con cada elemento del arreglo
     * @return La posicion en el arreglo, del valorBuscado
     */
    public static int busquedaLineal(int arreglo[], int valorBuscado) {
        int posicion = -1;
        for (int i = 0; i < arreglo.length; i++)
            if (valorBuscado == arreglo[i]){
                posicion =  i ;
                break;
            }
        return posicion; //Retorna -1 si no encuentra el valorBuscado
    }

    public static void main(String[] args) {
        int[] arreglo = {23, 25, 29, 42, 44, 46, 48, 49, 57, 59, 68, 74, 75, 85, 97};
        System.out.println("ARREGLO ORIGEN:");
        System.out.println(Arrays.toString(arreglo));
        int valorBuscado = 75;
        System.out.printf("%nElemento a buscar: [%d]%n", valorBuscado);
        System.out.println("\nBUSCANDO EL ELEMENTO EN:");
        System.out.println(Arrays.toString(arreglo));
        int posicionElemento = busquedaLineal(arreglo, valorBuscado);
        System.out.println(posicionElemento >= 0
                ? "Elemento [" + (valorBuscado) + "] encontrado en la posicion: ["
                + posicionElemento + "]" : "ELEMENTO [" + (valorBuscado) + "] NO ENCONTRADO");
    }
}
