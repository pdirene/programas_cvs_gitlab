package ARREGLOS;

import java.util.ArrayList;
import java.util.Collections;

class Persona implements Comparable<Persona> {

    public String nombre;
    public int edad, altura;

    public Persona(String nombre, int edad, int altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.altura = altura;
    }

    @Override
    public int compareTo(Persona o) {
        //return this.edad - o.edad;
        return this.nombre.compareTo(o.nombre);
    }

    @Override
    public String toString(){
        return String.format("Nombre: %s \t\t Edad: %d \t Altura: %d", this.nombre, this.edad, this.altura);
    }
}

public class Ordenar_Arreglo_Objetos {

    public static void main(String[] args) {
        ArrayList<Persona> personas = new ArrayList<Persona>();
        personas.add(new Persona("Mario", 90, 187));
        personas.add(new Persona("Pepe", 52, 173));
        personas.add(new Persona("Manuel", 27, 158));
        personas.add(new Persona("David", 25, 164));
        personas.add(new Persona("Alberto", 80, 184));

        System.out.println("Array sin ordenar por altura");
        for(Persona p : personas)
            System.out.println(p);
        //Ordeno el array de personas por altura (de menor a mayor).
        Collections.sort(personas);
        System.out.println("Array ordenado por altura");
        for(Persona p : personas)
            System.out.println(p);
    }
}
