/**
 * Metodo de ordenamieto Seleccion, para arreglos
 * @author Pedro Daniel Irene Robalino
 * @version 1.0.0
 */
package ARREGLOS;
import java.util.Arrays;

public class Ordenar_MetodoSeleccion {
    /**
     * Ordena un arreglo con el metodo de ordenamieto por seleccion
     * seleccionando el menor/mayor (>, <) elemento de todo el arreglo
     * para intercambiarlo al final de todas las comparaciones
     * @param arreglo Orginal a ser ordenado.
     * @return el arreglo ordenado.
 */
public static int[] seleccion(int[] arreglo) {
    for (int i = 0; i < arreglo.length - 1; i++) {
        int posmin = i;
        for (int j = i + 1; j < arreglo.length; j++)
            //< para orden Ascendente (de menor a mayor)
            //> para orden Descendente (de mayor a menor)
            if (arreglo[j] < arreglo[posmin])
                posmin = j; // posicion del mas pequeño
        intercambiar(arreglo, i, posmin);
    }
    return arreglo;
}

/**
 * Intercambia dos elementos del arreglo ha ser ordenado
 * @param arreglo Arreglo sub-ordenado
 * @param i posicion del primer elemento a ordenar
 * @param j posicion del segundo elemento a ordenar
 */
public static void intercambiar(int[] arreglo, int i, int j) {
    int aux = arreglo[i];
    arreglo[i] = arreglo[j];
    arreglo[j] = aux;
    System.out.printf("%d <=> %d   %s%n", arreglo[i], arreglo[j],
            Arrays.toString(arreglo));
}

    public static void main(String[] args) throws InterruptedException {
        int arreglo[] = {5, 6, 2, 1, 4}; //Arreglo de valores Original
        System.out.println("ARREGLO ORIGINAL");
        //Se presenta el arreglo original
        System.out.println(Arrays.toString(arreglo));
        //Se presenta todo el proceso de ordenamiento.
        System.out.println("\nORDENANDO EL ARREGLO");
        System.out.println(Arrays.toString(seleccion(arreglo)));
    }
}
