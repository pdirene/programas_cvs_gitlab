/**
 * Metodo de ordenamieto Burbuja, para arreglos
 * @author Pedro Daniel Irene Robalino
 * @version 1.0.0
 */
package ARREGLOS;
import java.util.Arrays;

public class Ordenar_MetodoBurbuja {
    /**
     * Ordena un arreglo con el metodo de ordenamieto Burbuja el cual burbujea
     * el mayor/menor elemento, al final del arreglo
     * @param arreglo Orginal a ser ordenado.
     * @return el arreglo ordenado.
     */
    public static int[] burbuja(int[] arreglo) {
        for (int i = 1; i < arreglo.length; i++) {
            for (int j = 0; j < arreglo.length - i; j++)
                //> para orden Ascendente (de menor a mayor)
                //< para orden Descendente (de mayor a menor)
                if (arreglo[j] > arreglo[j + 1])
                    intercambiar(arreglo, j, j + 1);
        }
        return arreglo;
    }

    /**
     * Intercambia dos elementos del arreglo ha ser ordenado
     * @param arreglo Arreglo sub-ordenado
     * @param i posicion del primer elemento a ordenar
     * @param j posicion del segundo elemento a ordenar
     */
    public static void intercambiar(int[] arreglo, int i, int j) {
        int aux = arreglo[i];
        arreglo[i] = arreglo[j];
        arreglo[j] = aux;
        System.out.printf("%d <=> %d   %s%n", arreglo[i], arreglo[j],
                Arrays.toString(arreglo));
    }

    public static void main(String[] args) throws InterruptedException {
        int arreglo[] = {5, 6, 2, 1, 4}; //Arreglo de valores Original
        System.out.println("ARREGLO ORIGINAL");
        //Se presenta el arreglo original
        System.out.println(Arrays.toString(arreglo));
        //Se presenta todo el proceso de ordenamiento.
        System.out.println("\nORDENANDO EL ARREGLO");
        System.out.println(Arrays.toString(burbuja(arreglo)));
    }
}
