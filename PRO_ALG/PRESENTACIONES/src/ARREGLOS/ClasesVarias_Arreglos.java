package ARREGLOS;

import java.util.Arrays;

public class ClasesVarias_Arreglos {

    public static void main(String[] args) {
        int elementoBuscar = 4;
        int arreglo[] = {5, 6, 2, 1, 4}; //Arreglo de valores Original
        System.out.println("ARREGLO ORIGINAL");
        //Convierta a String un arreglo. Útil para su presentación directa.
        System.out.println(Arrays.toString(arreglo));
        //Ordena arreglos de objetos
        //Collections.sort(List o ArrrayList de objetos);
        System.out.println("ARREGLO ORDENADO");
        //Ordena un arreglo.
        Arrays.sort(arreglo);
        System.out.println(Arrays.toString(arreglo));
        //Busca en un arreglo, un elemento especifico.
        System.out.printf("%d encontrado en la posición %d%n", elementoBuscar,
                Arrays.binarySearch(arreglo, elementoBuscar)+1);
    }
}
