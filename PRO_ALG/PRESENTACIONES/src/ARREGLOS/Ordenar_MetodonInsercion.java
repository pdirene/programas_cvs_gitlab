/**
 * Metodo de ordenamieto Insercion, para arreglos
 * @author Pedro Daniel Irene Robalino
 * @version 1.0.0
 */
package ARREGLOS;
import java.util.Arrays;

public class Ordenar_MetodonInsercion {
    /**
     * Ordena un arreglo con el metodo de ordenamieto por insercion
     * que va insertando el menor o mayor (<, >) en la posicion
     * en el orden que le corresponde.
     * @param arreglo Orginal a ser ordenado.
     * @return el arreglo ordenado.
     */
    public static int[] insercion(int[] arreglo) {
        for (int i = 1; i <= arreglo.length - 1; i++) {
            int insert = arreglo[i];
            int j = i - 1;
            //< para orden Ascendente (de menor a mayor)
            //> para orden Descendente (de mayor a menor)
            while (j >= 0 && insert < arreglo[j]) {
                arreglo[j + 1] = arreglo[j];
                System.out.printf("%d <=> %d  ", insert, arreglo[j]);
                j = j - 1;
            }
            arreglo[j + 1] = insert;
            System.out.printf("%n%s%n", Arrays.toString(arreglo));
        }
        return arreglo;
    }

    public static void main(String[] args) throws InterruptedException {
        int arreglo[] = {5, 6, 2, 1, 4}; //Arreglo de valores Original
        System.out.println("ARREGLO ORIGINAL");
        //Se presenta el arreglo original
        System.out.println(Arrays.toString(arreglo));
        //Se presenta todo el proceso de ordenamiento.
        System.out.println("\nORDENANDO EL ARREGLO");
        System.out.println(Arrays.toString(insercion(arreglo)));
    }
}
