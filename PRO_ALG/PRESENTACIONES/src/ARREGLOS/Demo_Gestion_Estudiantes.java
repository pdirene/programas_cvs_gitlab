/**
 * Demo_Gestion_Estudiantes usa las clases GestionArchivo_CSV y Estudiantes para
 * GestionArchivo_CSV: Generar calificaciones para una lista List de Estudiantes
 * Estudiantes: Cargar en esta clase, los datos (nombres y 2 notas) generados por
 * la clase GestionArchivo_CSV
 * @author Pedro Daniel Irene Robalino
 * @fecha  08-Ene-2020
 * @email  pdirene@gmail.com
 */

package ARREGLOS;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;


public class Demo_Gestion_Estudiantes {
    public static void main(String[] args) {
        int limNot = 2;
        String nombArchNotas = "Calificaciones.csv";
        ArrayList<Estudiantes> bdEst = new ArrayList<Estudiantes>();
        //Lista de nombres de estudiantes. Si se desea se puede agregar aqui mas estudiantes
        List<String> nombEst = Arrays.asList("Juan Carrion", "Pablo Pardo", "Ana Ruiz", "Zoila Zapata");
        //Crea el archivo de Salida y lo rellena con notas generadas aleatoriamente
        GestionArchivo_CSV gestion_FileCSV = new GestionArchivo_CSV(nombEst, limNot);
        gestion_FileCSV.generar_Archivo_CSV(nombArchNotas);
        //Lee el archivo CSV, deposita los datos en ArrayList
        gestion_FileCSV.leer_Archivo_CSV(nombArchNotas);
        //Crea un ArrayList de tipo Estidiantes con los datos del CSV
        System.out.println("LISTA DE ESTUDIANTES ORIGINALES/DESORDENADOS\n");
        System.out.printf("%-20s%-20s%-20s%n", "NOMBRES", "NOTA1", "NOTA2");
        System.out.printf("%-20s%-20s%-20s%n", "=======", "=====", "=====");
        bdEst = gestion_FileCSV.cargarDatosEstudiante();
        for(Estudiantes est : bdEst)
            System.out.printf("%-20s%-20.2f%-20.2f%n", est.nombres, est.nota1, est.nota2);
        //Ordena el ArrayList Estudiantes por el criterio del nombre del Estudiante
        System.out.println("\nESTUDIANTES ORDENADOS\n");
        Collections.sort(bdEst);
        System.out.printf("%-20s%-20s%-20s%n", "NOMBRES", "NOTA1", "NOTA2");
        System.out.printf("%-20s%-20s%-20s%n", "=======", "=====", "=====");
        for (Estudiantes est : bdEst)
            System.out.printf("%-20s%-20.2f%-20.2f%n", est.nombres, est.nota1, est.nota2);
    }
}

/**
 * Genera un archivo CSV y lo rellena con calificaciones generadas aletoriamente
 * dada una lista List de nombres de Estudiantes, para luego leer el CSV generado
 * y devolver un ArrayList de tipo Estudiante, con todos los datos precargados
 * para ser ordenados en la clase Demo_Gestion_Estudiantes
 * @author pdirene
 */
class GestionArchivo_CSV {

    public int limNot; //cantidad de calificaciones
    public int limEst; //cantidad de estudiantes
    public ArrayList<Estudiantes> bdEst; //Colleccion de objetos Estudiantes
    public ArrayList<Double> notasEst; //lista de calificaciones
    public List<String> nombEst; //lista de nombres de estudiantes
    /**
     * Inicializa los atributos de la clase GestionArchivo_CSV dado un nombre para
     * el archivo de salida CSV y un limite de calificaciones a generar, si el limite
     * de calificaciones a generar, se debe actualizar el metodo cargarDatosEstudiante
     * y la clase Estudiantes, para mas notas.
     * @param nombEst ArrayList con los nombres de cada Estudiante
     * @param limNot Limite ca calificaciones para cada estudiante
     */
    public GestionArchivo_CSV(List<String> nombEst, int limNot) {
        //copia la lista de nombres de estudiantes
        this.nombEst = new ArrayList<String>(nombEst);
        this.limNot = limNot;
        this.limEst = this.nombEst.size();
        this.notasEst = new ArrayList<Double>();
        this.bdEst = new ArrayList<Estudiantes>();
    }
    /**
     * Genera calificaciones reales entre 0 a 20 pts., las redondea a 2 cifras
     * para asignarlas al ArrayList notasEst. Mas adelante en el metodo
     * generar_Archivo_CSV este ArrayList sera escrito en un archivo CSV junto
     * a la lista List de nombres de Estudiantes nombEst.
     */
    public void generar_notas() {
        int notaDesde = 0, notaHasta = 20, numeroDecimales = 2;
        double nota = 0;
        for (int i = 0; i < limNot * limEst; i++) {
            //Genera una Calificacion.
            nota = (Math.random() * (notaHasta - (notaDesde - 1))) + notaDesde;
            //Redondea la calificacion generada anteriormente
            nota = Math.round(nota * Math.pow(10, numeroDecimales))
                    / Math.pow(10, numeroDecimales);
            //Calificacion generada y redondeada, se agrega al ArrayList
            notasEst.add(nota);
        }
    }
    /**
     * Escribe el List nombEst y el ArrayList notasEst en el archivo nombArchNotas
     * con las notas generadas en el metodo generar_notas()
     * @param nombArchNotas  Nombre del Archivo CSV de salida
     */
    public void generar_Archivo_CSV(String nombArchNotas) {
        try {
            //contEst y contNot: recorre los ArraysList notas y List nombEst
            //para escribirse en el archivo.
            int contEst = 1, contNot = 0;
            generar_notas();
            Locale ingles = new Locale("en", "EN");
            Formatter outArchivo = new Formatter(nombArchNotas, "US-ASCII", ingles);
            outArchivo.format("%s;%s;%s;%n", "NOMBRES", "NOTA1", "NOTA2");
            for (String est : nombEst) {
                outArchivo.format("%s;", est);
                while (contNot < (limNot * contEst)) {
                    outArchivo.format("%f;", notasEst.get(contNot));
                    contNot++;
                }
                outArchivo.format("%n");
                contEst++;
            }
            outArchivo.close();
        } catch (FileNotFoundException e) {
            System.err.println(e);
        } catch (UnsupportedEncodingException e) {
            System.err.println(e);
        }
    }
    /**
     * Lee un archivo .CVS con el siguiente formato
     *
     * NOMBRES;NOTA1;NOTA2;
     * nombEst1;valorNota1 del Est1;valorNota2 del Est1;
     * nombEst2;valorNota1 del Est2;valorNota2 del Est2;
     *
     * Y deposita su contenido la List nombEst en el ArrayList notasEst
     * @param nomArchNotas
     * @throws FileNotFoundException
     * @throws UnsupportedEncodingException
     */
    public void leer_Archivo_CSV(String nombArchNotas) {
        try {
            nombEst.clear();  //el ArraList de limpia para ser reutilizado
            notasEst.clear(); //el ArraList de limpia para ser reutilizado
            Scanner inArchivo = new Scanner(new File(nombArchNotas));
            String contenido;
            int nroEst = 0;
            inArchivo.nextLine(); //saltamos la lectura de la primera linea
            while (inArchivo.hasNext()) {
                //se lee desde la segunda linea
                contenido = inArchivo.nextLine();
                //separa en el array tokens[] cada dato segun un punto y coma (;)
                String tokens[] = contenido.split(";");
                //asigna en el ArrayList nombEst el nombre de cada Estudiante
                nombEst.add(tokens[0]);
                //copia en el array strNotas las notas de cada estudiante
                String strNotas[] = Arrays.copyOfRange(tokens, 1, (limNot + 1));
                //transforma el array de cadenas strNotas en doubles
                double douNotas[] = Arrays.stream(strNotas).mapToDouble
                        (Double::parseDouble).toArray();
                //cada nota es asignada al ArrayList notasEst
                for (double not : douNotas) {
                    notasEst.add(not);
                }
            }
            inArchivo.close();
        } catch (FileNotFoundException e) {
            System.err.println(e);
        }
    }
    /**
     * Carga el ArrayList bdEst con los objetos tipo Estudiante, que contienen
     * los nombres de cada estudiante (List nombEst) y calificaciones (ArrayList notasEst)
     * @return bdEst es un ArrayList con objetos de tipo Estudiante cargados.
     */
    public ArrayList<Estudiantes> cargarDatosEstudiante() {
        //contNot: recorre los ArraysList notasEst y List nombEst para escribirse
        //el ArrayList bdEst, que guarda todos los Estudiantes.
        int contNot = 0;
        for (String est : nombEst) {
            Estudiantes objEst = new Estudiantes();
            objEst.setNombres(est);
            objEst.setNota1(notasEst.get(contNot));
            contNot++;
            objEst.setNota2(notasEst.get(contNot));
            contNot++;
            bdEst.add(objEst);
        }
        return bdEst;
    }
}

/**
 * Estructura/clase para cargar los datos de un Estudiante (nombre, nota1 y nota2)
 * @author pdirene
 */
class Estudiantes implements Comparable<Estudiantes> {

    public String nombres; //nombre del estudiante
    double nota1, nota2;   // calificaciones de cada estudiante

    public Estudiantes() {}
    public Estudiantes(String nombre, double nota1, double nota2) {
        this.nombres = nombre;
        this.nota1 = nota1;
        this.nota2 = nota2;
    }
    public void setNombres(String nombres) {
        this.nombres = nombres;
    }
    public void setNota1(double nota1) {
        this.nota1 = nota1;
    }
    public void setNota2(double nota2) {
        this.nota2 = nota2;
    }
    /**
     * Ordena el ArrayList bdEst con los objetos de cada Estudiante
     * con el criterio de nombres.
     * @param e
     * @return
     */
    @Override
    public int compareTo(Estudiantes e) {
        //return this.nota1 - o.nota1;
        return this.nombres.compareTo(e.nombres);
    }
    @Override
    public String toString() {
        return String.format("Nombre: %s \t Nota1: %.2f \t Nota2: %.2f", nombres, nota1, nota2);
    }
}
