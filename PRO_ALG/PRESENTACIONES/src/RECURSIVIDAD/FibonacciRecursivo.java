package RECURSIVIDAD;
/**
 * La clase permite obtener la serie del fibonacci
 * @author Pedro Daniel Irene Robalino
 * @email pdirene@gmail.com
 */
public class FibonacciRecursivo {
    /**
     * Metodo recursivo para obtener el fibonacci
     * de una posicion numero) especifico.
     * @param numero
     * @return fibonacci de la posicion de numero
     */
    public static int FiboRecursivo(int numero){
        if (numero == 0 || numero == 1) //Evalua el caso base
            return numero; //Caso base
        else //Paso recursivo / llamada recursiva
            return FiboRecursivo(numero - 1) + FiboRecursivo(numero - 2);
    }
    public static void main(String args[]) {
        for (int i = 0; i <= 10; i++)
            System.out.printf("%-3d", FiboRecursivo(i));
    }
}
