package RECURSIVIDAD;

/**
 * La clase permite obtener el factorial de un número
 * recursivamente a traves del metodo recursivo(num)
 * @author Pedro Daniel Irene Robalino
 * @email pdirene@gmail.com
 */
public class FactorialRecursivo {
    public static void main(String[] args) {
        int num = 3, resultado = 0;
        resultado =factorialRecursivo(num);
        System.out.println(num + "! = " + resultado);
    }
    /**
     * Metodo recursivo para obtener el factorial de un numero
     * @param num por medio del cual se obtendra el factorial
     * @return retorna el factorial de num - 1
     */
    public static int factorialRecursivo(int num){
        if(num == 0) //Evalua el caso base
            return 1; //Caso base
        else { //Paso recursivo / llamada recursiva
            int devolver = (num * factorialRecursivo(num - 1));
            return devolver; //Vuelta recursiva
        }
    }
}
