package POLIMORFISMO;

import java.util.ArrayList;

public abstract class Animal {
    public int xCoordenada;
    public int yCoordenada;
    public abstract String mover();
}

class Pez extends Animal {
    public String mover() {
        return "Pez moviendo";
    }
}

class Rana extends Animal {
    public String mover() {
        return "Rana moviendo";
    }
}

class Ave extends Animal {
    public String mover() {
        return "Ave moviendo";
    }
}

class TestAnimal{
    public static ArrayList<Animal> animales = new ArrayList<Animal>();
    public static void main(String[] args) {
        Animal pez = new Pez();
        Animal rana = new Rana();
        Animal ave = new Ave();
        animales.add(pez);
        animales.add(rana);
        animales.add(ave);
        for (Animal animal : animales) {
            System.out.println(animal.mover());
        }
    }
}
