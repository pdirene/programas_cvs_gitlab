package POO;

public class Personas_2 {
    //Atributos ó campos
    public String sexo;
    protected double edad;
    public double estatura;
    private double peso;
    protected int numIdentidad;
    public String estado;
    //Constructores
    public Personas_2() { }
    public Personas_2(String sexo, double edad,
            double estatura, double peso,
            int numIdentidad, String estado) {
        this.sexo = sexo;
        this.edad = edad;
        this.estatura = estatura;
        this.peso = peso;
        this.numIdentidad = numIdentidad;
        this.estado = estado;
    }
    //Métodos SET (void), funciones GET (return)
    public String getSexo() {
        return sexo;
    }
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    public double getEdad() {
        return edad;
    }
    public void setEdad(double edad) {
        this.edad = edad;
    }
    public double getEstatura() {
        return estatura;
    }
    public void setEstatura(double estatura) {
        this.estatura = estatura;
    }
    public double getPeso() {
        return peso;
    }
    public void setPeso(double peso) {
        this.peso = peso;
    }
    public int getNumIdentidad() {
        return numIdentidad;
    }
    public void setNumIdentidad(int numIdentidad) {
        this.numIdentidad = numIdentidad;
    }
    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
}

class TestPersona{
    public static void main(String[] args) {
        Personas_2 personaA = new Personas_2();
        Personas_2 personaB = new Personas_2("M",34,170,
                75,1105678432,"Activo");
        personaA.setPeso(70);
        System.out.println("Peso de PersonaA "
                + "<"+personaA.getPeso()+">");
        System.out.println("Peso de PersonaB "
                + "<"+personaB.getPeso()+">");
    }
}