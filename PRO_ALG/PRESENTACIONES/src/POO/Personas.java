package POO;
public class Personas {
    //Atributos ó campos
    public String sexo;
    protected double edad;
    public double estatura;
    private double peso;
    protected int numIdentidad;
    public String estado;

    //Métodos (void), funciones (return)
    public void respirar() {
        estado = "respirando";
    }

    protected void caminar() {
        estado = "caminando";
    }

    private void alimentar() {
        estado = "alimentando";
    }

    public String investigar() {
        estado = "investigando";
        return estado;
    }
}

class TestPersonas{
    public static void main(String[] args) {
        Personas personaA = new Personas();
        Personas personaB = new Personas();
        Personas personaC = new Personas();
        personaA.sexo = "Masculino";
        personaA.caminar();
        System.out.println("personaA d sexo <"
                +personaA.sexo+"> esta <"
                +personaA.estado+">");
    }
}
