package POO;

public class Personas_5 {
    //Atributos ó campos
    public static String nombreEmpresa;
    public String sexo;
    public int edad;
    //Constructores
    public Personas_5() { }
    public Personas_5(String sexo, int edad) {
        nombreEmpresa = "UTPL";
        this.sexo = sexo;
        this.edad = edad;
    }
    //Métodos estaticos y no estaticos
    public static double calcularEdad(int anioNacimiento){
        int edad = 2020 - anioNacimiento;
        return edad;
    }
    public String getNombreEmpresa(){
        return nombreEmpresa;
    }
}

class TestPersonas_5{
    public static void main(String[] args) {
        System.out.println("Nombre de empresa de Personas <"+Personas_5.nombreEmpresa+">");
        Personas_5 personaA = new Personas_5("M",4);
        System.out.println("Nombre de empresa de Personas <"+Personas_5.nombreEmpresa+">");
        Personas_5 personaB = new Personas_5();
        System.out.println("Nombre de empresa de Personas <"+personaB.getNombreEmpresa()+">");
    }
}