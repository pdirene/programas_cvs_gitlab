package POO;

public class Personas_3 {
    //Atributos ó campos
    public String sexo;
    protected double edad;
    public double estatura;
    private double peso;
    protected int numIdentidad;
    public String estado;
    //Constructores
    public Personas_3() { }
    public Personas_3(String sexo, double edad,
                      double estatura, double peso,
                      int numIdentidad, String estado) {
        this.sexo = sexo;
        this.edad = edad;
        this.estatura = estatura;
        this.peso = peso;
        this.numIdentidad = numIdentidad;
        this.estado = estado;
    }
    //Métodos SET (void), funciones GET (return)
    public double calcularEdad(int anioNacimiento){
        int edad;
        edad = 2020 - anioNacimiento;
        this.edad = edad; //setEdad(edad);
        return edad;
    }
    public double getEdad() {
        return edad;
    }
    public void setEdad(double edad) {
        this.edad = edad;
    }
    public double getEstatura() {
        return estatura;
    }
    public void setEstatura(double estatura) {
        this.estatura = estatura;
    }
    public double getPeso() {
        return peso;
    }
    public void setPeso(double peso) {
        this.peso = peso;
    }
    public int getNumIdentidad() {
        return numIdentidad;
    }
    public void setNumIdentidad(int numIdentidad) {
        this.numIdentidad = numIdentidad;
    }
    public String getEstado() {
        return estado;
    }
    public void setEstado(String estado) {
        this.estado = estado;
    }
}

class TestPersonasV2{
    public static void main(String[] args) {
        Personas_3 personaA = new Personas_3();
        personaA.calcularEdad(1970);
        System.out.println("Edad de personaA <"
                +personaA.getEdad()+"> anios");
    }
}