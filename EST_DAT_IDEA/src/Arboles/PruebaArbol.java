package Arboles;

import java.util.Random;
// Fig. 17.17: Arbol.java
// Definición de las clases NodoArbol y Arbol.
// definición de la clase NodoArbol
class NodoArbol {
    // miembros de acceso del paquete
    int datos; // valor del nodo
    NodoArbol nodoIzq; // nodo izquierdo
    NodoArbol nodoDer; // nodo derecho
    public NodoArbol(int datosNodo) { // el constructor inicializa los datos y hace de este nodo un nodo raíz
        datos = datosNodo;
        nodoIzq = nodoDer = null; // el nodo no tiene hijos
    } // fin del constructor de NodoArbol
    // localiza el punto de inserción e inserta un nuevo nodo; ignora los valores duplicados
    public void insertar(int valorInsertar) {
        if (valorInsertar < datos) {  // inserta en el subárbol izquierdo
            if (nodoIzq == null) // inserta nuevo NodoArbol
                nodoIzq = new NodoArbol(valorInsertar);
            else // continúa recorriendo el subárbol izquierdo
                nodoIzq.insertar( valorInsertar );
        } // fin de if
        else if (valorInsertar > datos) {// inserta en el subárbol derecho
            if (nodoDer == null) // inserta nuevo NodoArbol
                nodoDer = new NodoArbol(valorInsertar);
            else // continúa recorriendo el subárbol derecho
                nodoDer.insertar( valorInsertar );
        } // fin de else if
    } // fin del método insertar
} // fin de la clase NodoArbol\
// definición de la clase Arbol
class Arbol {
    private NodoArbol raiz;
    public Arbol() { // el constructor inicializa un Arbol vacío de enteros
        raiz = null;
    } // fin del constructor de Arbol sin argumentos
    // inserta un nuevo nodo en el árbol de búsqueda binaria
    public void insertarNodo(int valorInsertar) {
        if (raiz == null)
            raiz = new NodoArbol(valorInsertar); // crea el nodo raíz aquí
        else
            raiz.insertar(valorInsertar); // llama al método insertar
    }// fin del método insertarNodo
    public void recorridoPreorden() {// comienza el recorrido preorden
        ayudantePreorden(raiz);
    } // fin del método recorridoPreorden
    private void ayudantePreorden(NodoArbol nodo) { // método recursivo para realizar el recorrido preorden
        if (nodo == null)
            return;
        System.out.printf("%d ", nodo.datos); // imprime los datos del nodo
        ayudantePreorden(nodo.nodoIzq);       // recorre el subárbol izquierdo
        ayudantePreorden(nodo.nodoDer);       // recorre el subárbol derecho
    } // fin del método ayudantePreorden
    // comienza recorrido inorden
    public void recorridoInorden() {
        ayudanteInorden(raiz);
    } // fin del método recorridoInorden
    private void ayudanteInorden(NodoArbol nodo) {// método recursivo para realizar el recorrido inorden
        if (nodo == null)
            return;
        ayudanteInorden(nodo.nodoIzq);          // recorre el subárbol izquierdo
        System.out.printf( "%d ", nodo.datos ); // imprime los datos del nodo
        ayudanteInorden(nodo.nodoDer);          // recorre el subárbol derecho
    } // fin del método ayudanteInorden
    public void recorridoPostorden() {  // comienza recorrido postorden
        ayudantePostorden(raiz);
    } // fin del método recorridoPostorden
    private void ayudantePostorden(NodoArbol nodo) { // método recursivo para realizar el recorrido postorden
        if (nodo == null)
            return;
        ayudantePostorden(nodo.nodoIzq);      // recorre el subárbol izquierdo
        ayudantePostorden(nodo.nodoDer);      // recorre el subárbol derecho
        System.out.printf("%d ", nodo.datos); // imprime los datos del nodo
    } // fin del método ayudantePostorden
} // fin de la clase Arbol

// Fig. 17.18: PruebaArbol.java
// Este programa prueba la clase Arbol.
public class PruebaArbol {
    public static void main(String args[]) {
        Arbol arbol = new Arbol();
        //int valor;
        Random numeroAleatorio = new Random();
        System.out.println("Insertando los siguientes valores: ");
        // inserta 10 enteros aleatorios de 0 a 99 en arbol
        //int valor[] = {10, 4, 63, 59, 80, 62, 3, 0, 36};
        int valor [] = {62, 46, 44, 77, 23, 66, 9, 81, 52 };
        for (int i = 0; i < 9; i++) {
            //valor = numeroAleatorio.nextInt(100);
            System.out.print(valor + " ");
            arbol.insertarNodo(valor[i]);
            //arbol.insertarNodo(valor[i]);
        } // fin de for
        System.out.println("\n\nRecorrido preorden");
        arbol.recorridoPreorden(); // realiza recorrido preorden de arbol System.out.println ( "\n\nRecorrido inorden" );
        System.out.println("\n\nRecorrido Inorden");
        arbol.recorridoInorden(); // realiza recorrido inorden de arbol
        System.out.println("\n\nRecorrido postorden");
        arbol.recorridoPostorden(); // realiza recorrido postorden de arbol System.out.println();
        System.out.println();
    } // fin de main
} // fin de la clase PruebaArbol

