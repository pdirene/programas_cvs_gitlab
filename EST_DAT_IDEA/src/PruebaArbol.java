//package Arbol;
class Nodo{
    int i; Nodo nI, nD;
    public Nodo(int i){
        this.i = i; this.nI = this.nD = null;
    }
}
class Arbol{
    public Nodo raiz, nn;
    public void insertarNodo(int i){
        nn = new Nodo(i);
        if (raiz == null) {
            raiz = nn;
            System.out.format("%s \t %d \t %s\n", "nR",nn.i, nn);
        }
        else {
            Nodo na = raiz, np;
            while (true) {
                np = na;
                if (i < np.i) {
                    na = na.nI;
                    if (na == null) {
                        np.nI = nn;
                        System.out.format("%s \t %d \t %s\n","nI", nn.i, nn);
                        return;
                    }
                } else {
                    na = na.nD; // nn = n1.sgt;
                    if (na == null) {
                        np.nD = nn;
                        System.out.format("%s \t %d \t %s\n","nD", nn.i, nn);
                        return;
                    }
                }
            }
        }
    }
    public void preOrden(Nodo nodo){
        if (nodo == null) return;
        else{
            System.out.print(nodo.i + ", ");
            preOrden(nodo.nI);
            preOrden(nodo.nD);
        }
    }
}
public class PruebaArbol {
    public static void main(String[] args) {
        int valor[] = {8, 3, 1, 6, 4, 7, 10, 14, 13};
        Arbol arbol = new Arbol();
        //arbol.insertarNodo(valor[0]);
        for (int i = 0; i < 9; i++)
            arbol.insertarNodo(valor[i]);
        System.out.println("\nRECORRIDO PreOrden");
        arbol.preOrden(arbol.raiz);
        System.out.println();
    }
}
